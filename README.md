# 白狐DNF单机版1.0

#### 介绍
白狐DNF单机版1.0 是一个白狐DNF单机的基础版本，采用Lua + Galaxy2D Game Engine开发的DNF同人游戏。

#### 软件架构

ArcticFox_DNF_1.0 采用 [Galaxy2D Game Engine](https://pan.baidu.com/s/14a0VgqmFRXSz_gqhEolmrg "Galaxy2D Game Engine")开发。

你可以下载它，然后自行修改做成自己的游戏。

#### 其他说明

1. 文件提取码：`l4dx`
